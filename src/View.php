<?php

namespace Sulfur\Paragraph;


class View
{

	public function url($name)
	{
		if($name === '/') {
			return trim(get_site_url(), '/') . '/';
		}
	}


	public function src($id, $size)
	{
		$data = wp_get_attachment_image_src($id, $size);
		if(is_array($data) && isset($data[0])) {
			return $data[0];
		}
	}


	public function pagination($previous = '&laquo;', $next = '&raquo;')
	{
		$pagination = paginate_links([
			'type' => 'array',
			'prev_text' => $previous,
			'next_text' => $next,
		]);

		return $pagination ? $pagination : [];
	}


	public function all($type, $filter = [])
	{
		$items = [];
		$taxonomies = Paragraph::config('taxonomy');
		$args = [
			'post_type' => $type,
			'tax_query' => [],
		];
		foreach($filter as $key => $value) {
			if(isset($taxonomies[$key])) {
				$args['tax_query'][] = [
					'taxonomy' => $key,
					'terms' => $value
				];
			}
		}
		$posts = get_posts($args);
		foreach($posts as $post) {
			$items[] = Paragraph::entity($post);
		}
		return $items;
	}


	public function comments($item)
	{
		if(comments_open($item->id) || get_comments_number($item->id)) {

			$comments = wp_list_comments([
				'per_page' => 10000,
				'echo' => false
			]);

			ob_start();
			comment_form();
			$form = ob_get_clean();

			return [
				'comments' => $comments,
				'form' => $form,
			];
		}

		return [
			'comments' => '',
			'form' => '',
		];
	}


	public function option($name, $default = null)
	{
		return get_option($name, $default);
	}


	public function wp_lang()
	{
		ob_start();
		language_attributes();
		return ob_get_clean();
	}


	public function wp_body_class()
	{
		ob_start();
		body_class();
		$class = ob_get_clean();
		return str_replace(['class="', '"'], '', $class);
	}


	public function wp_charset()
	{
		ob_start();
		bloginfo( 'charset' );
		return ob_get_clean();
	}


	public function wp_header($title = null)
	{
		add_filter('wpseo_title', '__return_empty_string');

		add_filter('document_title_parts', function($parts) use ($title){
			if($parts['title'] == 'Pagina niet gevonden') {
				$parts['title'] = $title;
			}
			return $parts;
		}, 999);

		ob_start();
		wp_head();
		return ob_get_clean();
	}


	public function wp_footer()
	{
		ob_start();
		wp_footer();
		return ob_get_clean();
	}

	
	public function search()
	{
		ob_start();
		get_search_form();
		return ob_get_clean();
	}
}