<?php
$item = Paragraph::one();
if($body = Paragraph::view(get_post_type() . '/item', ['item' => $item])) {
	echo $body;
} else {
	echo Paragraph::view('default/item', ['item' => $item]);
}