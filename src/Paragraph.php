<?php

namespace Sulfur\Paragraph;

use Sulfur\App;

class Paragraph
{
	public static function bootstrap()
	{
		if (class_exists('Bootstrap')) {
			$bootstrap = new \Bootstrap();
			$methods = get_class_methods('Bootstrap');
			foreach ($methods as $method) {
				if ($method !== '__construct') {
					$bootstrap->{$method}();
				}
			}
		}
	}


	public static function init()
	{
		self::bootstrap();
		add_action('admin_init', function() {
			foreach (static::config('paragraph/option') as $id => $settings) {
				register_setting('general', $id, 'esc_attr');
				add_settings_field(
					$id, '<label for="' . $id . '">' . __($settings['name']) . '</label>', function() use ($id, $settings) {
					$value = get_option($id, '');
					echo '<input type="' . $settings['type'] . '" id="' . $id . '" name="' . $id . '" value="' . esc_attr($value) . '" />';
				}, 'general'
				);
			}
		});


		// PL TODO allow custom gallery styling
		add_filter('use_default_gallery_style', '__return_false');

		// JK TODO set deafult body tekst input type
		add_filter('wp_default_editor', function() {
			return 'tinymce';
		});

		// MH TODO Add theme support
		add_filter('tiny_mce_before_init', function($options, $foo) {
			$options['extended_valid_elements'] = 'span[class|id|style|y-use|data-*],div[class|id|style|y-use|data-*]';
			return $options;
		}, 10, 5);




		// MH TODO Add theme support
		add_theme_support('html5');
		add_theme_support('title-tag');

		// Flush cache on admin pageload
		add_action('admin_init', function() {
			$cache = static::container('Sulfur\Paragraph\Cache');
			$cache->flush();
		});




		// Set image sizes
		foreach (static::config('paragraph/image') as $id => $image) {
			add_image_size($id, $image[0], $image[1], isset($image[2]) ? $image[2] : false );
		}


		// Add sidebars
		foreach (static::config('paragraph/sidebar') as $id => $sidebar) {
			$sidebar['name'] = __($sidebar['title'], 'paragraph');
			$sidebar['id'] = $id;
			$sidebar['description'] = __($sidebar['description'], 'paragraph');
			register_sidebar($sidebar);
		}

		// Add menus
		foreach (static::config('paragraph/menu') as $id => $description) {
			register_nav_menu($id, __($description, 'paragraph'));
		}


		// Add Yellow javascript
		add_action('admin_footer', function () {
			$base = trim(get_site_url(), '/') . '/';
			$src = $base . 'vendor/yellow/Yellow.js';
			$main = $base . (static::config('env', 'name') == 'development' ? 'js.php?rebuild=paragraph' : 'js/paragraph.js');
			$source = $base . (static::config('env', 'name') == 'development' ? 'js.php?build=paragraph&file=' : 'js/');
			$console = static::config('env', 'name') == 'development' ? '1' : '0';
			$tag = '<div y-use="paragraph.Main"></div><script type="text/javascript"
				src="' . $src . '"
				data-main="' . $main . '"
				data-src="' . $source . '"
				data-console="' . $console . '"
			 ></script>';
			echo $tag;
		});


		add_action('init', function() {

			// get types
			$types = static::config('paragraph/type');


			// Register taxonomies
			$taxonomies = static::config('paragraph/taxonomy', []);
			foreach (static::config('paragraph/taxonomy', []) as $taxonomy => $config) {

				// Taxonomy language
				$lang = [
					'name' => 'item',
					'singular' => 'item',
					'plural' => 'items',
					'new' => 'new',
					'description' => 'generic item'
				];

				if (isset($config['lang']) && is_array($config['lang'])) {
					$lang = array_merge($lang, $config['lang']);
				}

				$from = [];
				$to = [];
				foreach ($lang as $key => $val) {
					$full['{{' . $key . '}}'] = $val;
					$full['{{!' . $key . '}}'] = ucfirst($val);
				}
				$from = array_keys($full);
				$to = array_values($full);

				$replace = function($string) use ($from, $to) {
					return __(str_replace($from, $to, $string), 'paragraph');
				};

				$config['labels'] = [
					'name' => $replace('{{!name}}'),
					'singular_name' => $replace('{{!singular}}'),
					'menu_name' => $replace('{{!name}}'),
					'all_items' => $replace('Alle {{plural}}'),
					'parent_item' => $replace('Parent {{!singular}}'),
					'parent_item_colon' => $replace('Parent {{!singular}} :'),
					'new_item_name' => $replace('Nieuwe {{singular}} naam'),
					'add_new_item' => $replace('{{!new}} {{singular}}'),
					'edit_item' => $replace('{{!singular}} bewerken'),
					'update_item' => $replace('{{!singular}} bewerken'),
					'view_item' => $replace('{{!singular}} bekijken'),
					'separate_items_with_commas' => $replace('Scheiden met komma\'s'),
					'add_or_remove_items' => $replace('{{!plural}} verwijderen of toevoegen'),
					'choose_from_most_used' => $replace('Kies uit meest gebruikte {{plural}}'),
					'popular_items' => $replace('Populaire {{plural}}'),
					'search_items' => $replace('{{!plural}} zoeken'),
					'not_found' => $replace('{{!singular}} niet gevonden'),
					'no_terms' => $replace('Geen {{plural}}'),
					'items_list' => $replace('Lijst'),
					'items_list_navigation' => $replace('Navigatielijst'),
				];

				// Get types for which to register
				$forTypes = [];
				foreach ($types as $type => $typeConfig) {
					if (isset($config['taxonomies']) && is_array($config['taxonomies']) && in_array($taxonomy, $config['taxonomies'])) {
						$forTypes[] = $type;
					}
				}

				register_taxonomy($taxonomy, $forTypes, $config);
			}


			// Register post types
			foreach ($types as $type => $config) {
				if ($type !== 'post' && $type !== 'page') {

					$config = array_merge([
						'public' => true,
						'has_archive' => true,
						'hierarchical' => true
						], $config);


					$lang = [
						'name' => 'item',
						'singular' => 'item',
						'plural' => 'items',
						'new' => 'new',
						'description' => 'generic item'
					];

					if (isset($config['lang']) && is_array($config['lang'])) {
						$lang = array_merge($lang, $config['lang']);
					}

					$from = [];
					$to = [];
					foreach ($lang as $key => $val) {
						$full['{{' . $key . '}}'] = $val;
						$full['{{!' . $key . '}}'] = ucfirst($val);
					}
					$from = array_keys($full);
					$to = array_values($full);

					$replace = function($string) use ($from, $to) {
						return __(str_replace($from, $to, $string), 'paragraph');
					};

					$config['labels'] = [
						'name' => $replace('{{!name}}'),
						'singular_name' => $replace('{{!singular}}'),
						'add_new' => $replace('{{!new}} {{singular}}'),
						'add_new_item' => $replace('{{!new}} {{singular}}'),
						'edit_item' => $replace('{{!singular}} bewerken'),
						'new_item' => $replace('{{!new}} {{singular}}'),
						'view_item' => $replace('{{!singular}} bekijken'),
						'view_items' => $replace('{{!plural}} bekijken'),
						'search_items' => $replace('{{!plural}} zoeken'),
						'not_found' => $replace('{{!singular}} niet gevonden'),
						'parent_item_colon' => $replace('Parent {{!singular}}'),
						'all_items' => $replace('Alle {{plural}}'),
						'archives' => $replace('Alle artikelen'),
						'attributes' => $replace('{{!singular}} attributen'),
						'insert_into_item' => $replace('In {{singular}} plaatsen'),
						'uploaded_to_this_item' => $replace('Naar {{singular}} ge-upload'),
						'menu_name' => $replace('{{!name}}'),
					];
					register_post_type($type, $config);
				}
			}
		});



		// Register metaboxes
		add_filter('rwmb_meta_boxes', function ($boxes) {
			$types = static::config('paragraph/type');
			foreach (static::config('paragraph/metabox') as $id => $box) {
				$box['id'] = $id;
				$box['post_types'] = [];
				foreach ($types as $type => $config) {
					if (isset($config['metabox']) && in_array($id, $config['metabox'])) {
						$box['post_types'][] = $type;
					}
				}

				foreach ($box['fields'] as $id => $field) {
					$box['fields'][$id]['id'] = $id;
					if (strpos($field['type'], 'Metafield\\') === 0) {
						$type = str_replace('\\', '', $field['type']);
						$classname = 'RWMB_' . $type . '_Field';
						if (!class_exists($classname)) {
							eval('class ' . $classname . ' extends \\' . $field['type'] . '{}');
						}
						$box['fields'][$id]['type'] = $type;
					}
				}
				$boxes[] = $box;
			}
			return $boxes;
		});



		// Register widgets
		add_action('widgets_init', function () {
			$widgets = static::config('paragraph/widget');
			$existing = array_keys($GLOBALS['wp_widget_factory']->widgets);

			// shut down default widgets
			foreach (array_diff($existing, $widgets) as $widget) {
				unregister_widget($widget);
			}

			// enable configured widgets
			foreach (array_diff($widgets, $existing) as $widget) {
				register_widget($widget);
			}
		});


		add_filter('template_redirect', function() {
			// before a render, check if there is a sulfur route that is matched
			$request = Paragraph::container('Sulfur\Request');
			$router = Paragraph::container('Sulfur\Router');
			if ($router->match($request->path(false), $request->method(), $request->domain())) {
				// if so: dont redirect permalinks to somewhere else
				remove_filter('template_redirect', 'redirect_canonical');
				// instead: run sulfur
				add_action('template_redirect', function() {
					Paragraph::app()->handleHTTP();
					exit;
				});
			}
		}, 0);
	}


	public static function app()
	{
		static $app;
		if (is_null($app)) {
			$app = new App(require ROOT . 'config/config.php', require ROOT . 'config/env.php');
		}
		return $app;
	}


	// Container helper function
	public static function container($classOrCallable = null, $params = [], $context = null)
	{
		static $container;
		if (is_null($container)) {
			$container = static::app()->container();
		}

		if ($classOrCallable === null) {
			return $container;
		} elseif ($context === null) {
			return $container->get($classOrCallable, $params);
		} else {
			return $container->call($classOrCallable, $params, $context);
		}
	}


	// Config helper function
	public static function config($resource = null, $key = null, $default = null)
	{
		static $config;
		if (is_null($config)) {
			$config = static::container('Sulfur\Config');
		}

		if ($resource === null) {
			return $config;
		} else {
			return $config->resource($resource, $key, $default);
		}
	}


	// Helper function
	public static function view($file = null, $values = [])
	{
		$cache = static::container('Sulfur\Paragraph\Cache');
		static $view;
		if (is_null($view)) {
			$view = static::container('Sulfur\View');
			// TODO: move this to configurable spot
			$helper = static::container('View\Helper');
			$helper->register($view);
		}
		if ($file === null) {
			return $view;
		} else {
			$body = $view->render($file, $values);
			$cache->page($body);
			return $body;
		}
	}


	public static function one($postId = null, $type = '')
	{
		if ($postId === null) {
			the_post();
			$post = get_post();
		} elseif (is_numeric($postId)) {
			$post = get_post($postId);
		} elseif (is_string($postId)) {
			$posts = get_posts([
				'name' => $postId,
				'posts_per_page' => 1,
				'post_type' => $type,
				'post_status' => 'publish'
			]);
			if (isset($posts[0])) {
				$post = $posts[0];
			} else {
				return null;
			}
		} else {
			return null;
		}
		return static::entity($post);
	}


	public static function all($query = null)
	{
		$items = [];
		if ($query === null) {
			while (have_posts()) {
				$items[] = static::one();
			}
		} else {
			while ($query->have_posts()) {
				$query->the_post();
				$items[] = static::one(get_the_ID());
			}
		}
		return $items;
	}


	public static function entity($post)
	{
		$class = 'Entity\\' . ucfirst($post->post_type);
		if (class_exists($class)) {
			$instance = static::container($class);
		} else {
			$instance = static::container('Sulfur\Paragraph\Entity');
		}
		$instance->prepare($post);
		return $instance;
	}


	public static function cache()
	{
		$cache = static::container('Sulfur\Paragraph\Cache');
		if ($body = $cache->page()) {
			echo $body;
			exit;
		}
	}
}
