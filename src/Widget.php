<?php

namespace Sulfur\Paragraph;

use WP_Widget;

class Widget extends WP_Widget
{
	function __construct()
	{
		parent::__construct($this->type, $this->name, [
			'description' => isset($this->description) ? $this->description : null
		]);
	}


	function widget($args, $instance)
	{
		$instance = is_array($instance) ? $instance : [];
		$values = [];
		foreach($this->elements($instance) as $el) {
			if(isset($el[0])) {
				$key = $el[0];
				if(isset($instance[$key])) {
					$values[$key] = $instance[$key];
				} elseif(isset($el['default'])) {
					$values[$key] = $el['default'];
				} else {
					$values[$key] = null;
				}
			}
		}
		echo Paragraph::view()->render($this->view, ['item' => (object) $values]);
	}


	function update($new, $old)
	{
		$new = is_array($new) ? $new : [];
		$values = [];
		foreach($this->elements($new) as $el) {
			if(isset($el[0])) {
				$key = $el[0];
				if(isset($new[$key])) {
					$values[$key] = $new[$key];
				} elseif(isset($el['default'])) {
					$values[$key] = $el['default'];
				} else {
					$values[$key] = null;
				}


				if(isset($el[1])) {
					$type = $el[1];
					if($type == 'url') {
						if(strlen($values[$key]) > 0 && strpos($values[$key], 'http') !== 0 && strpos($values[$key], '//') !== 0 ) {
							$values[$key] = '//' . $values[$key];
						}
					}

					if($type == 'link') {
						$value = json_decode($values[$key]);
						$value->href = isset($value->type) && isset($value->href) && $value->type == 'manual' ? $value->href : get_permalink($value->id);
						$values[$key] = $value;


					}
				}
			}
		}
		return $values;
	}


	function form($instance)
	{

		$instance = is_array($instance) ? $instance : [];
		$elements = [];
		foreach($this->elements($instance) as $el) {
			if(isset($el[0]) && isset($el[1])) {
				$element = [
					'value' => isset($instance[$el[0]]) ? $instance[$el[0]] : null,
					'name' => $this->get_field_name(array_shift($el)),
					'type' => array_shift($el),
				];
				if($element['type'] == 'image') {
					$element['src'] = wp_get_attachment_image_src($element['value'], 'thumbnail')[0];
				}

				if($element['type'] == 'link' && $element['value'] == null) {
					$element['value'] = [
						'type' => 'manual',
						'href' => '',
						'target' => '_self'
					];
				}

				$element = array_merge($element, $el);

				$elements[] = (object) $element;
			}
		}
		echo Paragraph::view()->render('paragraph/widget', ['elements' => $elements]);
	}
}
