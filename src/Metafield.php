<?php

namespace Sulfur\Paragraph;

use RWMB_Field;

class Metafield extends RWMB_Field
{


	public static $instance = null;


	protected static function instance()
	{
		if(static::$instance === null) {
			static::$instance = new static();
		}

		return static::$instance;
	}


	public static function value($new, $old, $post_id, $field)
	{
		return static::instance()->process($new);
	}



	public static function meta($post_id, $saved, $field)
	{
		$meta = parent::meta($post_id, $saved, $field);
		return static::instance()->prepare($meta);

	}


	public static function html($meta, $field)
	{
		$element = $field;
		$element['value'] = $meta;
		$element['type'] = static::instance()->type();
		return Paragraph::view()->render('paragraph/metafield', ['element' => (object) $element]);
	}

	public $type = '';

	public function process($value)
	{
		return $value;
	}

	public function type()
	{
		return $this->type;
	}

	public function prepare($value)
	{
		return $value;
	}
}