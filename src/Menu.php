<?php

namespace Sulfur\Paragraph;

class Menu
{

	public function __construct()
	{

	}

	public function get($name)
	{
		$items = [];
		if(is_numeric($name)) {
			$items = wp_get_nav_menu_items($name);
		} else {
			$menus = get_nav_menu_locations();
			if(isset($menus[$name])) {
				$items = wp_get_nav_menu_items($menus[$name]);
			}
		}

		if(is_array($items)) {
			$tree = [];
			$index = [];
			foreach($items as $item) {
				$leaf = (object) [
					'title' => $item->title,
					'url' => $item->url,
					'children' => []
				];

				$index[$item->ID] = $leaf;
				if(isset($index[(int) $item->menu_item_parent ])) {
					$index[(int) $item->menu_item_parent ]->children[] = $leaf;
				} else {
					$tree[] = $leaf;
				}
			}
			return $tree;
		} else {
			return [];
		}
	}

}
