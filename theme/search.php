<?php
echo Paragraph::view('search/index', [
	'items' => Paragraph::all(),
	'form' => get_search_form(false)
]);