<?php

namespace Sulfur\Paragraph;

class Sidebar
{

	public function __construct()
	{

	}

	public function get($name, $raw = false)
	{
		if($raw) {
			global $wp_registered_widgets;
			$sidebars = wp_get_sidebars_widgets();
			$widgets = [];
			if(isset($sidebars[$name])) {
				foreach($sidebars[$name] as $id) {
					if(isset($wp_registered_widgets[$id])) {
						$data = $wp_registered_widgets[$id];
						$object = $data['callback'][0];
						$widget = (object) get_option($object->option_name) [$data['params'][0]['number']];
						$widget->type = $object->id_base;
						ob_start();
						call_user_func($data['callback'], [
							'before_title'  => '',
							'after_title'   => '<!--[[break]]-->',
							'before_widget'   => '',
							'after_widget'   => '',
						], $data['params'][0]);
						$html =  ob_get_clean();
						$parts = explode('<!--[[break]]-->', $html);
						if(count($parts) >= 2) {
							$title = $parts[0];
							$body = $parts[1];
						} else {
							$title = '';
							$body = $html;
						}
						$widget->html = (object) [
							'title' => $title,
							'body' => $body,
						];
						$widgets[] = $widget;
					}
				}
			}
			return $widgets;
		} else {
			ob_start();
			dynamic_sidebar($name);
			return ob_get_clean();
		}
	}



}
