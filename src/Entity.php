<?php

namespace Sulfur\Paragraph;

use WP_Query;
use Exception;

class Entity
{
	protected $data = [];

	protected $post;


	public function prepare($post)
	{

		$this->post = $post;

		$this->data['id'] =  $post->ID;
		$this->data['title'] =  $post->post_title;
		$this->data['type'] =  $post->post_type;
		$this->data['body'] = apply_filters('the_content', $post->post_content);
		$this->data['url'] = get_permalink($post->ID);
		$this->data['time'] = strtotime($post->post_date);
	}


	public function post()
	{
		return $this->post;
	}


	public function __get($name)
	{
		if(isset($this->data[$name])) {
			return $this->shortcode($this->data[$name]);
		} elseif($value = rwmb_meta($name, [], $this->data['id'])) {
			if(is_string($value)) {
				$decoded = json_decode($value, true);
				if(is_array($decoded)) {
					return (object) $decoded;
				}
			}
			return $this->shortcode($value);
		} elseif(method_exists($this, $name)){
			return $this->shortcode($this->{$name}());
		}
	}


	protected function shortcode ($content){

		if(is_string($content)) {
			global $wp_embed;
			//https://wordpress.stackexchange.com/questions/22524/do-shortcode-not-working-for-embed
			$content = $wp_embed->run_shortcode($content);
			$content = do_shortcode($content);
			return $content;
		} else {
			return $content;
		}
	}


	protected function author()
	{
		return (object) [
			'avatar' =>  get_the_author_meta ('avatar', $this->post->post_author),
			'name' => get_the_author_meta ('display_name', $this->post->post_author)
		];
	}


	public function __call($name, $args)
	{
		if(isset($args[0]) && is_array($args[0])) {
			$options = $args[0];
		} else {
			$options = [];
		}
		$value = rwmb_meta($name, $options, $this->data['id']);
		if($value || is_array($value)) {
			return $value;
		}
	}
}