<?php

$object = get_queried_object();

$content = [
	'items' => Paragraph::all(),
	'taxonomy' => null
];


if (isset($object->taxonomy)) {
	$object->description = wpautop($object->description);
	$content['taxonomy'] = $object;
	$taxonomies = Paragraph::config('paragraph/taxonomy');
	if(isset($taxonomies[$object->taxonomy]) && isset($taxonomies[$object->taxonomy]['type'])) {
		echo Paragraph::view($taxonomies[$object->taxonomy]['type'] . '/index', $content);
	} elseif($object->taxonomy == 'category' || $object->taxonomy == 'post_tag') {
		echo Paragraph::view('post/index', $content);
	} else {
		echo Paragraph::view('default/index', $content);
	}
} elseif ($type = $object->name) {
	if($body = Paragraph::view($type . '/index', $content) ) {
		echo $body;
	} else {
		echo Paragraph::view('default/index', $content);
	}
} else {
	echo Paragraph::view('home/index', $content);
}