<?php

namespace Sulfur\Paragraph;

use Sulfur\Cache as BaseCache;
use Sulfur\Request;

class Cache
{

	/**
	 * request instance
	 * @var Sulfur\Request
	 */
	protected $request;

	/**
	 * cache instance
	 * @var Sulfur\Cache
	 */
	protected $cache;


	protected $version = '1';


	public function __construct(Request $request, BaseCache $cache)
	{
		$this->request = $request;
		$this->cache = $cache;
		$this->version = (int) $this->cache->get('cache_version', 1);
	}

	public function page($body = null)
	{
		$allowed = true;
		$path =  $this->request->path();
		if(strpos($path, 'wp-') !== false) {
			$allowed = false;
		} elseif(! empty ($this->request->post())) {
			$allowed = false;
		} elseif($this->authenticated()) {
			$allowed = false;
		}

		if($allowed) {
			$key = 'page_' . $this->version . '_' . $path;
			if($body == null) {
				return $this->cache->get($key, null);
			} else {
				$this->cache->set($key, $body);
			}
		}
	}


	public function flush()
	{
		 $version = (int) $this->cache->get('cache_version', 1);
		 $version++;
		 $this->cache->set('cache_version', $version);
	}


	protected function authenticated()
	{
		foreach($_COOKIE as $key => $val) {
			if(strpos($key, 'wordpress_logged_in') !== false && strlen($val) > 10) {
				return true;
			}
		}
		return false;
	}
}